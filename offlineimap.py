#!/usr/bin/python
# coding: utf8
"""OfflineImap functions"""

import keyring


def get_usr(account):
    """Get email address for an account"""
    return keyring.get_password(account, "login")


def get_pwd(account):
    """Get password from keyring"""
    return keyring.get_password(account, "password")

outlook_names = [
    ("Brouillons", "Drafts"),
    ("&AMk-l&AOk-ments envoy&AOk-s", "Sent"),
    ("&AMk-l&AOk-ments supprim&AOk-s", "Trash"),
    ("Courrier ind&AOk-sirable", "Junk"),
]

def nametrans_outlook(foldername):
    for pair in outlook_names:
        if foldername == pair[0]:
            return pair[1]
    return foldername


def revert_nametrans_outlook(foldername):
    for pair in outlook_names:
        if foldername == pair[1]:
            return pair[0]
    return foldername
