scriptencoding utf-8

" Mieg's vim configuration file
"
" Copyright Rémy Taymans <remytaymans@gmail.com>
"
" To the extent possible under law, the author(s) have dedicated all
" copyright and related and neighboring rights to this software to the
" public domain worldwide. This software is distributed without any
" warranty.
"
" You should have received a copy of the CC0 Public Domain Dedication
" along with this software. If not, see
" http://creativecommons.org/publicdomain/zero/1.0/.


" Prevent Vim from modifying terminal window title.
set notitle

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
" Required when using Vundle
set nocompatible

" Set encoding
"set encoding=utf-8
"set fileencodings=utf-8

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? 'evim'
    finish
endif

" Set leader key
let g:mapleader='è'

" Bépo keybord map
" Commented because of the usage of vim-bepo plugin
"source ~/.vimrc.bepo

" Usage of Vundle Package Manager
filetype off
" Vundle setup
" required!
set runtimepath+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle
" required!
Plugin 'VundleVim/Vundle.vim'

" List of bundle
Plugin 'tpope/vim-fugitive' " A Git wrapper
Plugin 'sheerun/vim-polyglot' " A set of syntax for specific language
"Plugin 'leafgarland/typescript-vim'
"Plugin 'lumiliet/vim-twig'
"Plugin 'jvirtanen/vim-octave'
Plugin 'godlygeek/tabular' " Needed for markdown syntax 'plasticboy/vim-markdown'
"Plugin 'gabrielelana/vim-markdown' " syntax highlighting for markdown
Plugin 'dpelle/vim-Grammalecte' " French grammar corrector
Plugin 'w0rp/ale' " Allow you to lint when your typing
Plugin 'tpope/vim-surround' " Mappings to easily delete, change and add surroundings in pairs.
"Plugin 'tpope/vim-sleuth' "Automatically adjusts indentation type (space, tabs)
Plugin 'scrooloose/nerdtree' " File Tree view for vim
Plugin 'xuyuanp/nerdtree-git-plugin' " Git plugin for NERDTree
Plugin 'majutsushi/tagbar' " Show tags (ctags and other) in a pannel
Plugin 'remytms/vim-bepo' " Configuration for bepo keyboard
Plugin 'junegunn/fzf' " Plugin fzf for the console
Plugin 'junegunn/fzf.vim' " Plugin fzf for vim
"Plugin 'jremmen/vim-ripgrep' " Use rg to search term in buffers
Plugin 'mileszs/ack.vim' " Plugin to search term in files (also with rg)
Plugin 'justinmk/vim-dirvish' " File tree for vim
Plugin 'LucHermitte/lh-vim-lib' " Library for local_vimrc
Plugin 'LucHermitte/local_vimrc' " Load per project vim configuration
Plugin 'vim-scripts/loremipsum' " Insert loremipsum text
Plugin 'vim-airline/vim-airline' " Lean & mean status/tabline for vim that's light as air
"Plugin 'ctrlpvim/ctrlp.vim' " Full path fuzzy file, buffer, mru, tag, ... finder for Vim
Plugin 'yggdroot/indentline' " Display the indention levels with vertical lines
Plugin 'psf/black' " Paint python code in black (python formatter)
Plugin 'fisadev/vim-isort' " Sort python import statement
"Plugin 'editorconfig/editorconfig-vim' " Read .editorconfig file

call vundle#end()
filetype plugin indent on " required!

" Grammalecte configuration
let g:grammalecte_cli_py='~/.vim/grammalecte/pythonpath/cli.py'

" Local Vim configuration
let g:local_vimrc = ['.vimrc.local', '_vimrc_local.vim']
let g:local_vimrc_options = {}
let g:local_vimrc_options.whitelist = ['/home/remy/dev', '/home/remy/Programmation/']

" Airline configuration
"let g:airline#extensions#tabline#enabled = 1 " Enable the list of buffers

" Vim ALE (linter for vim) configuration
let g:ale_sign_column_always = 1
let g:ale_sign_error = '>'
let g:ale_sign_warning = '-'
hi clear ALEError
hi clear ALEWarning
hi ALEWarning ctermfg=white ctermbg=blue
hi ALEError ctermfg=black ctermbg=red
let g:ale_fixers = {
\   '*': ['remove_trailing_lines'],
\   'python': ['isort'],
\}

" fzf - Fuzzy Finder plugin
set runtimepath+=~/.fzf

" fzf keybinding
" Stand for <leader>goto
nnoremap <leader>g :Buffers<cr>
" Stand for <leader>windows
nnoremap <leader>w :Windows<cr>
" Stand for <leader>new
nnoremap <leader>n :Files
" Stand for <leader>Rg
nnoremap <leader>r :Rg<cr>
" Movement in insert mode
inoremap <C-d> <Up>
inoremap <C-s> <Down>
inoremap <C-t> <Left>
inoremap <C-r> <Right>
" Indenting in visual mode (keeping selected)
vnoremap » >gv
vnoremap « <gv

" ctrlp configuration
let g:ctrlp_user_command = 'fd --type f "" %s'
"nnoremap <leader>n :CtrlP<cr>

" ack configuration
if executable('rg')
  let g:ackprg = 'rg --vimgrep --no-heading --color=always --ignore-case'
endif

" NERDTree configuration
let g:NERDTreeWinSize=22  " This let me split the rest of the vue with 80 * 2
let g:NERDTreeMapOpenVSplit = "vs"
" The two following keybinding seams to work like simple s and d key.
let g:NERDTreeMapJumpNextSibling = "C-s"
let g:NERDTreeMapJumpPrevSibling = "C-d"
nnoremap <leader>tt :NERDTreeToggle<cr>
nnoremap <leader>tf :NERDTreeFind<cr>
nnoremap <leader>tg :NERDTreeFocus<cr>

" Vim-markdown
" The following has nothing to do with markdown, but it fuckup the
" conceal of markdown file
let g:indentLine_conceallevel = 1
"let g:vim_markdown_conceal = 0
" The following is for the 'gabrielelana/vim-markdown', but the one in
" use is the one from polyglot (sea above)
"let g:markdown_enable_conceal = 0

" TagBar
" Search for tag in the current directory, and work up the tree until
" one is found
set tags=./tags;/

" Black configuration
let g:black_linelength=79

" Buffer configuration
nnoremap <leader>é :bn<cr>
nnoremap <leader>b :bp<cr>

" Insert carriage return in normal mode
" Do the reverse of {J}
nnoremap <leader>s i<cr><esc>

" Search other occurrence of the selection in visual mode
vnoremap <leader>/ y/\V<C-r>=escape(@",'/\')<cr><cr>

" Insert breakpoint for python script
" Already used !!
"nnoremap <leader>b oimport pdb; pdb.set_trace()<esc>
"nnoremap <leader>B Oimport pdb; pdb.set_trace()<esc>

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

" Use backup during editing but delete it when closing
set nobackup
set writebackup

set hidden " Allow to change between buffer without saving them

set history=50      " keep 50 lines of command line history
set ruler           " show the cursor position all the time
set showcmd         " display incomplete commands
set incsearch       " do incremental searching

set number          " affiche les numéro de la ligne
set relativenumber  " affiche les numéro de ligne de manière relative a
                    " la ligne courante
" On colorie les nombre en gris
hi LineNr ctermfg=8
hi CursorLineNr cterm=bold ctermfg=8
set showcmd		    " affiche la commande que l'on est en train de tapper
set spell spelllang=en,fr,eo " Corriger les fautes d'orthographe
" On colorie les fautes d'orthorgrahpe
hi SpellBad cterm=none ctermfg=red ctermbg=none
hi SpellLocal cterm=italic ctermbg=none
set backspace=2     " Le retour chariots recule de 2 espaces
" Histoire d'avoir un bon affichage du texte digne d'un IDE
set autoindent      " On indente automatiquement
set breakindent     " Si un retour à la ligne est imposé, il est fait
                    " de manière à respecter l'indentation
set showbreak=\ \\_ " Lors d'un retour à la ligne forcé, on affiche ces
                    " caractères
set linebreak       " Coupe les lignes trop longue au bon endroit et pas
                    " au milieu d'un mot
set textwidth=72    " On limite la largeur d'une ligne en nombre de
                    " caractère
" On colorie la colone limite. Il y a 3 colones de dessinées.
" La première à 73 (72 + 1) caractère. La deuxième à 80 (79 + 1). Et une
" autre qui correspond à textwith +1. Très pratique dans le cas où l'on
" défini un textwidth différent de celui de ce fichier.
set colorcolumn=73,80,+1  " On colorie la colone limite
" On fixe la couleur de la colone limite en gris
hi ColorColumn ctermbg=8 guibg=lightgrey

" Histoire d'avoir une bonne indentation
set shiftwidth=4	" how many columns text is indented with the reindent
                    " operations (<< and >>)
set tabstop=4       " how many columns a tab counts for
set softtabstop=2   " how many columns vim uses when you hit Tab in insert mode
set smarttab        " J'pense bien que c'est important ^^
set expandtab       " hitting Tab in insert mode will produce the appropriate
                    " number of spaces
set listchars=space:·,tab:>-,nbsp:~ " Configure how to highlight space and tabs
set list            " Highlight spaces and tabs
" Configure color of highlighting
hi NonText ctermfg=8 guifg=lightgray
hi SpecialKey ctermfg=8 guifg=lightgray
" Formatting options
" Default is tcq (see fo-table)
set formatoptions+=w " Dont wrap line ending for example with an
                     " html tag

" Enable folding
set foldmethod=indent
set foldlevel=99
hi Folded ctermfg=2 ctermbg=none
" Enable folding with the spacebar
nnoremap <space> za
nnoremap   zA

" Session configuration
set sessionoptions-=option    " do not store global and local values in a session
set sessionoptions-=folds     " do not store folds

" Don't use Ex mode, use Q for formatting
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
    set mouse=a
endif

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has('gui_running')
    syntax on
    set hlsearch
endif

" Only do this part when compiled with support for autocommands.
if has('autocmd')

    " Enable file type detection.
    " Use the default filetype settings, so that mail gets 'tw' set to 72,
    " 'cindent' is on in C files, etc.
    " Also load indent files, to automatically do language-dependent indenting.
    filetype plugin indent on

    "Specific indentation for special file
    autocmd FileType html,xml,xsd,dtd,xslt,twig,yaml setlocal tabstop=2 shiftwidth=2

    " Function to delete trailing space for all typefile except some
    fun! StripTrailingWhitespace()
        " Don't strip on these filetypes
        " syntax is 'language\|othe_language\|third_language'
        if &filetype =~ 'markdown'
            return
        endif
        %s/\s\+$//e
    endfun

    " This delete trailing space when a file is saved
    autocmd BufWritePre * call StripTrailingWhitespace()

    " For all text files set 'textwidth' to 72 characters.
    autocmd FileType text,markdown setlocal textwidth=72

    " When editing a file, always jump to the last known cursor position.
    " Don't do it when the position is invalid or when inside an event handler
    " (happens when dropping a file on gvim).
    " Also don't do it when the mark is in the first line, that is the default
    " position when opening a file.
    autocmd BufReadPost *
                \ if line("'\"") > 1 && line("'\"") <= line("$") |
                \   exe "normal! g`\"" |
                \ endif

else

    set autoindent		" always set autoindenting on

endif " has("autocmd")

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(':DiffOrig')
    command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
                \ | wincmd p | diffthis
endif

filetype off
set runtimepath+=/usr/share/lilypond/2.14.2/vim/
filetype on

" Some specific syntax coloration
"au BufNewFile,BufRead *.less set filetype=css " Not needed since i got
                                               " syntax for it
au BufNewFile,BufRead *.twig set filetype=html
