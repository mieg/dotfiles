# Hooks for alot mail

import tempfile
import subprocess

import alot
from alot.helper import string_sanitize
from alot.helper import string_decode


def refresh_search(ui, currentpos=None):
    """Refresh a SearchBuffer UI but keeping the current position."""
    if currentpos is None:
        thread = ui.current_buffer.get_selected_thread()
        for pos, tlw in enumerate(ui.current_buffer.threadlist.get_lines()):
            if tlw.get_thread().get_thread_id() == thread.get_thread_id():
                currentpos = pos
    ui.current_buffer.rebuild()
    ui.update()
    if currentpos:
        ui.current_buffer.body.set_focus(currentpos)


def archive_thread(ui=None):
    """
    Mark a thread as archive and remove inbox, spam, and killed tag.
    Equivalent to tag archive; untag inbox,spam,killed;
    But this doesn't work, because when tagging archive, the thread is
    removed from the search and therefore the untag command acts on the
    next thread.
    See: https://github.com/pazz/alot/issues/769
    """
    if ui is None:
        return
    thread = ui.current_buffer.get_selected_thread()
    if not thread:
        return
    thread.add_tags({"archive"})
    thread.remove_tags({"inbox", "spam", "killed"})
    ui.dbman.flush()
    refresh_search(ui)


def unarchive_thread(ui=None):
    """
    Unarchive a thread and move it into inbox.
    Similar to archive_thread(). See documentation there.
    """
    if ui is None:
        return
    thread = ui.current_buffer.get_selected_thread()
    if not thread:
        return
    thread.add_tags({"inbox"})
    thread.remove_tags({"archive", "spam", "killed"})
    ui.dbman.flush()
    refresh_search(ui)


def sync_mail(ui=None):
    """
    Sync email with offlineimap.
    """
    msg = ui.notify("Syncing emails...", timeout=-1)
    process = subprocess.Popen("syncmail".split(),
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    process.wait()
    ui.clear_notify([msg])
    ui.notify("Syncing emails is done.")
    ui.current_buffer.rebuild()


def quick_pull_mail(ui):
    """
    Only fetch new mails with offlineimap.
    """
    msg = ui.notify("Quick fetchinig emails...", timeout=-1)
    process = subprocess.Popen("syncmail".split(),
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    process.wait()
    ui.clear_notify([msg])
    ui.notify("Quick fetching emails is done.")
    ui.current_buffer.rebuild()


def _get_raw_html(msg):
    """
    Helper method to extract the raw html part of a message. Note that it
    only extracts the first text/html part found.
    """
    mail = msg.get_email()

    for part in mail.walk():
        ctype = part.get_content_type()

        if ctype != "text/html":
            continue

        cd = part.get('Content-Disposition', '')

        if cd.startswith('attachment'):
            continue

        enc = part.get_content_charset() or 'utf-8'

        raw = string_decode(part.get_payload(decode=True), enc)

        return string_sanitize(raw)

    return None


def open_in_browser(ui=None):
    """
    Open html emails in an external viewer.
    """
    ui.notify("Opening message in browser...")
    msg = ui.current_buffer.get_selected_message()

    htmlmail = _get_raw_html(msg)

    if htmlmail is None:
        ui.notify("Email has no html part")
        return

    temp = tempfile.NamedTemporaryFile(prefix="alot-", suffix=".html",
                                       delete=False)
    temp.write(htmlmail.encode("utf-8"))
    temp.flush()
    temp.close()
    # Other browser can be used. Choose the one you prefer.
    # subprocess.Popen(
    #     ("midori --private file://%s" % temp.name).split(),
    #     stdout=subprocess.PIPE,
    #     stderr=subprocess.PIPE
    # )
    # subprocess.Popen(
    #     ("firefox --private-window file://%s" % temp.name).split(),
    #     stdout=subprocess.PIPE,
    #     stderr=subprocess.PIPE
    # )
    subprocess.Popen(
        ("chromium --incognito file://%s" % temp.name).split(),
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE
    )
