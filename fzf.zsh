# Setup fzf
# ---------
if [[ ! "$PATH" == */home/remy/.fzf/bin* ]]; then
  export PATH="${PATH:+${PATH}:}/home/remy/.fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/remy/.fzf/shell/completion.zsh" 2> /dev/null

# Key bindings
# ------------
source "/home/remy/.fzf/shell/key-bindings.zsh"
